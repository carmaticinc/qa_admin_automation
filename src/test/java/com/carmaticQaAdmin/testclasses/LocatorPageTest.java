package com.carmaticQaAdmin.testclasses;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.carmaticQaAdmin.pages.LocatorPage;

public class LocatorPageTest extends BaseTest
{
	protected String className = this.getClass().getSimpleName();
	protected String envt,actualValue="Compare";
	
	@BeforeClass
    @Parameters(value = {"browserName", "environment"})
    public void setUp(String browserName, String environment) throws Exception
    {
        envt = environment;
        System.out.println("Browser Name is : "+browserName);
        System.out.println("Environment is : "+envt);
        System.out.println("Class Name is : "+className);
        init(browserName, envt, className);
    }
	
	@Test(priority=1)
    public void TestLogin() throws Exception 
    {
        extentReportUtil.reportInfo("Execution Started on "+envt, driver);
        performLogin();
        Thread.sleep(5000);
    }
	
	@Test(priority=2)
	public void LocatorPage() throws Exception
	{
		//System.out.println("#### From Locator Page ####");
		LocatorPage locatorpage = new LocatorPage(elementUtil, driver);
		locatorpage.mouseOverOnVehicle(driver, className);
		locatorpage.selectLocator(driver, className);Thread.sleep(3000);
		locatorpage.verifyLocatorPage(driver, actualValue, className);
		locatorpage.clickPagination(driver, className);
		System.out.println("Now you are in the page(Title): "+driver.getTitle());
		System.out.println("#### End of Locator Page ####");	
		Thread.sleep(5000);
	}
	
	@AfterClass
	public void tearDown()throws Exception 
    {
    	driver.quit();
    	endTest();
    }

}
