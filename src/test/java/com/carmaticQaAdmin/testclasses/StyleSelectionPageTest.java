package com.carmaticQaAdmin.testclasses;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.carmaticQaAdmin.pages.HomePage;
import com.carmaticQaAdmin.pages.StyleSelectionPage;

public class StyleSelectionPageTest extends BaseTest
{
	
	public String className = this.getClass().getSimpleName();
	public String envt,actualValue="Compare";
	
	@BeforeClass
    @Parameters(value = {"browserName", "environment"})
    public void setUp(String browserName, String environment) throws Exception
    {
        envt = environment;
        System.out.println("Browser Name is : "+browserName);
        System.out.println("Environment is : "+envt);
        System.out.println("Class Name is : "+className);
        init(browserName, envt, className);
    }
	
	@Test(priority=1)
    public void TestLogin() throws Exception 
    {
        extentReportUtil.reportInfo("Execution Started on "+envt, driver);
        performLogin();
        Thread.sleep(5000);
    }
	
	@Test(priority=2)
	public void StyleSelectionPage() throws Exception
	{
		//System.out.println("#### From Style Selection Page ####");
		StyleSelectionPage styleselectionpage = new StyleSelectionPage(elementUtil, driver);
		HomePage homepage = new HomePage(elementUtil, driver);
		homepage.clickAnyManufacturers(driver, className);
		Thread.sleep(3000);
		homepage.clickAnyModel(driver, className);
		Thread.sleep(3000);
		styleselectionpage.verifyStyleSelectionPage(driver, actualValue, className);
		styleselectionpage.clickAnyStyle(driver, className);
		System.out.println("#### End of Style Selection Page ####");
		Thread.sleep(3000);
	}
	
	@AfterClass
	public void tearDown()throws Exception 
    {
    	driver.quit();
    	endTest();
    }

}
