package com.carmaticQaAdmin.testclasses;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.carmaticQaAdmin.pages.HomePage;

public class HomePageTest extends BaseTest
{
	public String className = this.getClass().getSimpleName();
	public String envt,actualValue="Compare",year="2019",make="AUDI",model="A3";
	
	@BeforeClass
    @Parameters(value = {"browserName", "environment"})
    public void setUp(String browserName, String environment) throws Exception
    {
        envt = environment;
        System.out.println("Browser Name is : "+browserName);
        System.out.println("Environment is : "+envt);
        System.out.println("Class Name is : "+className);
        init(browserName, envt, className);
    }
	
	@Test(priority=1)
    public void TestLogin() throws Exception 
    {
        extentReportUtil.reportInfo("Execution Started on "+envt, driver);
        performLogin();
        Thread.sleep(5000);
    }
	
	@Test(priority=2)
	public void homePage() throws Exception
	{
		//System.out.println("#### From Home Page ####");
		HomePage homepage = new HomePage(elementUtil, driver);
		homepage.verifyHomePage(driver, actualValue, className);
		homepage.clickOnYearDropDown(driver, className);
		homepage.selectYear(driver, className, year);
		Thread.sleep(5000);
		homepage.clickOnMakeDropDown(driver, className);
		homepage.selectMake(driver, className, make);
		Thread.sleep(5000);
		homepage.clickOnModelDropDown(driver, className);
		homepage.selectModel(driver, className, model);
		homepage.clickOnFilteredCar(driver, className);
		//homepage.clickAnyManufacturers(driver, className);
		System.out.println("#### End of Home Page ####");
		Thread.sleep(5000);
		
	}
	
	
	@AfterClass
	public void tearDown()throws Exception 
    {
    	driver.quit();
    	endTest();
    }
	
}
