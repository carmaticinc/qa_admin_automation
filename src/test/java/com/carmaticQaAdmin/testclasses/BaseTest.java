package com.carmaticQaAdmin.testclasses;

import org.openqa.selenium.WebDriver;

import com.carmaticQaAdmin.pages.LoginPage;
import com.carmaticQaAdmin.util.ElementUtil;
import com.carmaticQaAdmin.util.ExtentReportUtil;

public class BaseTest 
{
	public 	  ExtentReportUtil extentReportUtil;

    protected String        baseUrl;
    protected String        userName;
    protected String        password1;
    protected String 		reportTitle;
    protected WebDriver     driver;
    protected ElementUtil   elementUtil;
    protected String 		methodName, className="LoginTest_From_BaseTest";
    
    
    public void init(String browserName, String environment, String reportTitle) 
    {
    	System.out.println("From BASE TEST BrowserName is : "+browserName);
    	extentReportUtil    = new ExtentReportUtil();
    	elementUtil         = new ElementUtil(extentReportUtil);
    	driver              = elementUtil.createDriver(browserName);
        baseUrl             = "https://qa.admin.carmatic.com/login";
        userName        	= "Developer";
        password1         	= "Carmatic@123"; 
        extentReportUtil.startTest(reportTitle);
    }
    
    public void performLogin() throws Exception
    {
    	LoginPage loginPage = new LoginPage(elementUtil, driver);
    	driver.get(baseUrl);    	
    	Thread.sleep(3000);
    	loginPage.enterUserName(driver,userName,className);   	
    	loginPage.enterUserPassword(driver,password1,className);
    	loginPage.clickOnSubmitButton(driver, className);
    	
    }
    
    public void endTest()
    {
        driver.quit();
        extentReportUtil.endTest();
    }
    
    
}
