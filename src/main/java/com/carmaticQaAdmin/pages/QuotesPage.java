package com.carmaticQaAdmin.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.carmaticQaAdmin.util.ElementUtil;

public class QuotesPage extends BasePage
{

	public QuotesPage(ElementUtil elementutil, WebDriver driver) {
		super(elementutil, driver);
		PageFactory.initElements(driver, this);
	}

	// Quotes Page Locators
	
			@FindBy(xpath="//i[@class='site-menu-icon md-money']")
			public WebElement sales;
			
			@FindBy(xpath="//span[contains(text(),'Quotes')]")
			public WebElement quotes;
			//li[1]//ul[1]//li[1]//a[1]//span[1]
			
			@FindBy(xpath="//h1[@class='page-title']")
			public WebElement compareLink;
			
			@FindBy(xpath="//div[contains(text(),'This Year')]")
			public WebElement thisyear;
			
			public String methodName;
			
			
			public void mouseOverOnSales(WebDriver driver,String className) throws Exception
			{
				methodName = new Throwable().getStackTrace()[0].getMethodName();
				elementutil.click(driver, sales, methodName, className);
			}
			
			public void selectQuotes(WebDriver driver,String className) throws Exception
			{
				methodName = new Throwable().getStackTrace()[0].getMethodName();
				elementutil.click(driver, quotes, methodName, className);
			}
			
			public void verifyQuotesPage(WebDriver driver,String text,String className) throws Exception
			{
				methodName = new Throwable().getStackTrace()[0].getMethodName();
				elementutil.verifyText(driver, compareLink, text, methodName, className);
			}
			
			public void clickThisYear(WebDriver driver,String className) throws Exception
			{
				methodName = new Throwable().getStackTrace()[0].getMethodName();
				elementutil.click(driver, thisyear, methodName, className);
			}
			
			
}
