package com.carmaticQaAdmin.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.carmaticQaAdmin.util.ElementUtil;

public class StyleSelectionPage extends BasePage
{

	public StyleSelectionPage(ElementUtil elementutil, WebDriver driver) {
		super(elementutil, driver);
		PageFactory.initElements(driver, this);
	}
	
	// Style Selection Page Locators  
		@FindBy(xpath="//span[contains(text(),'BACK TO MODELS')]")
		public WebElement compareLink;
		
		@FindBy(xpath="//div[@id='pcks']//div[1]//div[2]//div[2]//a[1]")
		public WebElement selectStyle;
		
		public String methodName;
		
		
		public void verifyStyleSelectionPage(WebDriver driver,String text,String className) throws Exception
		{
			methodName = new Throwable().getStackTrace()[0].getMethodName();
			elementutil.verifyText(driver, compareLink, text, methodName, className);
		}
		
		public void clickAnyStyle(WebDriver driver,String className) throws Exception
		{
			methodName = new Throwable().getStackTrace()[0].getMethodName();
			elementutil.click(driver, selectStyle, methodName, className);
		}

}
