package com.carmaticQaAdmin.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.carmaticQaAdmin.util.ElementUtil;

public class LocatorPage extends BasePage
{

	public LocatorPage(ElementUtil elementutil, WebDriver driver) {
		super(elementutil, driver);
		PageFactory.initElements(driver, this);
	}

	// Locator Page objects  
			
			@FindBy(xpath="//i[@class='site-menu-icon md-car']")
			public WebElement vehicle;
			
			@FindBy(xpath="//span[contains(text(),'Locator')]")
			public WebElement locator;
			
			@FindBy(xpath="//label[contains(text(),'Bad/Incomplete reocrds')]")
			public WebElement compareLink;
			
			@FindBy(xpath="//div[@id='car-table_paginate']//a[contains(text(),'2')]")
			public WebElement paginationcheck;
			
			
			public String methodName;
			
			
			public void mouseOverOnVehicle(WebDriver driver,String className) throws Exception
			{
				methodName = new Throwable().getStackTrace()[0].getMethodName();
				elementutil.click(driver, vehicle, methodName, className);
			}
			
			public void selectLocator(WebDriver driver,String className) throws Exception
			{
				methodName = new Throwable().getStackTrace()[0].getMethodName();
				elementutil.click(driver, locator, methodName, className);
			}
			
			public void verifyLocatorPage(WebDriver driver,String text,String className) throws Exception
			{
				methodName = new Throwable().getStackTrace()[0].getMethodName();
				elementutil.verifyText(driver, compareLink, text, methodName, className);
			}
			
			public void clickPagination(WebDriver driver,String className) throws Exception
			{
				methodName = new Throwable().getStackTrace()[0].getMethodName();
				elementutil.click(driver, paginationcheck, methodName, className);
			}
			
			
}
