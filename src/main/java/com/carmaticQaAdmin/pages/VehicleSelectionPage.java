package com.carmaticQaAdmin.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.carmaticQaAdmin.util.ElementUtil;

public class VehicleSelectionPage extends BasePage
{

	public VehicleSelectionPage(ElementUtil elementutil, WebDriver driver) {
		super(elementutil, driver);
		PageFactory.initElements(driver, this);
	}
	
	// Vehicle Selection Page Locators 
		@FindBy(xpath="//h4[contains(text(),'Filter by Option')]")
		public WebElement compareLink;
		
		@FindBy(xpath="//div[@class='w']//div[1]//div[2]//div[2]//a[1]")
		public WebElement selectVehicle;
		
		@FindBy(xpath="//tr[1]//td[4]//div[1]//a[1]")
		public WebElement WorkDealButton;
		
		public String methodName;
		
		
		public void verifyVehicleSelectionPage(WebDriver driver,String text,String className) throws Exception
		{
			methodName = new Throwable().getStackTrace()[0].getMethodName();
			elementutil.verifyText(driver, compareLink, text, methodName, className);
		}
		
		public void clickAnyVehicle(WebDriver driver,String className) throws Exception
		{
			methodName = new Throwable().getStackTrace()[0].getMethodName();
			elementutil.click(driver, selectVehicle, methodName, className);
		}
		
		public void clickWorkDealButton(WebDriver driver,String className) throws Exception
		{
			methodName = new Throwable().getStackTrace()[0].getMethodName();
			elementutil.click(driver, WorkDealButton, methodName, className);
		}

}
