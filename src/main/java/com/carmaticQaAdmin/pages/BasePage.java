package com.carmaticQaAdmin.pages;

import org.openqa.selenium.WebDriver;

import com.carmaticQaAdmin.util.ElementUtil;


public class BasePage 
{
	protected ElementUtil elementutil;
	protected WebDriver driver;
	
	// Constructor
	public BasePage(ElementUtil elementutil,WebDriver driver)
	{
		this.elementutil = elementutil;
		this.driver = driver;
	}
}
