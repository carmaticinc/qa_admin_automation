package com.carmaticQaAdmin.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.carmaticQaAdmin.util.ElementUtil;

public class HomePage extends BasePage {

	public HomePage(ElementUtil elementutil, WebDriver driver) {
		super(elementutil, driver);
		PageFactory.initElements(driver, this);
	}

	// Home Page Locators
	@FindBy(xpath = "//a[@id='compare-btn']")
	public WebElement compareLink;

	@FindBy(xpath = "//div[@title='AUDI']")
	public WebElement audi;

	@FindBy(xpath = "//a[1]//div[1]//img[1]")
	public WebElement anymodel;
	
	@FindBy(xpath = "//div[@class='img-w']//img")
	public WebElement filteredcar;
	

	@FindBy(xpath = "//button[@title='YEAR']")
	public WebElement yeardropdown;

	@FindBy(xpath = "//button[@title='MAKE']")
	public WebElement makedropdown;

	@FindBy(xpath = "//button[@title='MODEL']")
	public WebElement modeldropdown;

	@FindBy(xpath = "//div[@class='dropdown-menu open']/ul")
	List<WebElement> uls;

	public String yearxp = "//*[@id='year-wrap']//ul/li[";
	public String makexp = "//*[@id='mfg-wrap']//ul/li[";
	public String modelxp = "//*[@id='model-wrap']//ul/li[";
	public String xp = "]";
	public String methodName;

	public void verifyHomePage(WebDriver driver, String text, String className) throws Exception {
		methodName = new Throwable().getStackTrace()[0].getMethodName();
		elementutil.verifyText(driver, compareLink, text, methodName, className);
	}

	public void clickAnyManufacturers(WebDriver driver, String className) throws Exception {
		methodName = new Throwable().getStackTrace()[0].getMethodName();
		elementutil.click(driver, audi, methodName, className);
	}
	
	  public void clickAnyModel(WebDriver driver,String className) throws Exception
	  { methodName = new Throwable().getStackTrace()[0].getMethodName();
	  elementutil.click(driver, anymodel, methodName, className); }
	 

	public void clickOnYearDropDown(WebDriver driver, String className) throws Exception {
		methodName = new Throwable().getStackTrace()[0].getMethodName();
		elementutil.click(driver, yeardropdown, methodName, className);
	}

	public void selectYear(WebDriver driver, String className, String value) {
		methodName = new Throwable().getStackTrace()[0].getMethodName();
		elementutil.selectValueFromDropDown(value, yearxp, xp, driver, uls, methodName, className);
	}

	public void clickOnMakeDropDown(WebDriver driver, String className) throws Exception {
		methodName = new Throwable().getStackTrace()[0].getMethodName();
		elementutil.click(driver, makedropdown, methodName, className);
	}

	public void selectMake(WebDriver driver, String className, String value) {
		methodName = new Throwable().getStackTrace()[0].getMethodName();
		elementutil.selectValueFromDropDown(value, makexp, xp, driver, uls, methodName, className);
	}

	public void clickOnModelDropDown(WebDriver driver, String className) throws Exception {
		methodName = new Throwable().getStackTrace()[0].getMethodName();
		elementutil.click(driver, modeldropdown, methodName, className);
	}

	public void selectModel(WebDriver driver, String className, String value) {
		methodName = new Throwable().getStackTrace()[0].getMethodName();
		elementutil.selectValueFromDropDown(value, modelxp, xp, driver, uls, methodName, className);
	}
	
	public void clickOnFilteredCar(WebDriver driver, String className) throws Exception {
		methodName = new Throwable().getStackTrace()[0].getMethodName();
		elementutil.click(driver, filteredcar, methodName, className);
	}

}
