package com.carmaticQaAdmin.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.carmaticQaAdmin.util.ElementUtil;

public class LoginPage extends BasePage
{

	
	public LoginPage(ElementUtil elementutil, WebDriver driver) 
	{
		super(elementutil, driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//input[@id='Username']")
	public WebElement userName;
	
	@FindBy(xpath="//input[@id='Password']")
	public WebElement password;
	
	@FindBy(xpath="//button[@id='submit-form']")
	public WebElement submitButton;
	
	public String methodName;
	
	
	public void enterUserName(WebDriver driver,String text,String className) throws Exception
	{
		methodName = new Throwable().getStackTrace()[0].getMethodName();
		elementutil.writeText(driver,userName,methodName, text,className);
	}
	
	public void enterUserPassword(WebDriver driver,String text,String className)throws Exception
	{
		methodName = new Throwable().getStackTrace()[0].getMethodName();
		//System.out.println(text);
		elementutil.writeText(driver,password,methodName,text,className);
	}
	
	
	public void clickOnSubmitButton(WebDriver driver,String className) throws Exception
	{
		methodName = new Throwable().getStackTrace()[0].getMethodName();
		elementutil.click(driver, submitButton, methodName, className);
	}
	
}
