package com.carmaticQaAdmin.util;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/*
 *  Created By Aruntej.Goli on 14/12/2019
 */
public class ElementUtil 
{

	public 					String 				gatheredText;
	protected 				Actions 			action;
	protected 				Capabilities 		cap;
	protected 				ExtentReportUtil 	extentreport;
	protected 				WebDriverWait 		wait;
	public 					String 				SEPARATOR = System.getProperty("file.separator");
	public 					String 				driverFolder = "/drivers";
	public  				int 				timeoutInSec = 40;
	
	
	public ElementUtil(ExtentReportUtil extentreport)
	{
		this.extentreport = extentreport;
	}
	
	public void openUrl(WebDriver driver,String url,String methodName,String className) throws Exception
	{
		try 
		{
			driver.navigate().to(url);
			extentreport.reportPass(" Successfully Executed "+methodName +" Method from the class " +className ,methodName,driver);
		}
		catch (Exception e) 
		{
			extentreport.reportFail(" Error Occurred during the execution of   "+methodName +" Method from the class " +className,methodName,driver);
			System.out.println(e.getMessage());
		}
	}
	public void click(WebDriver driver,WebElement element,String methodName,String className) throws Exception
	{
		try
		{
			element.click();
			extentreport.reportPass(" Successfully Executed "+methodName +" Methd from the class " +className ,methodName,driver);
		}
		catch (Exception e) 
		{
			extentreport.reportFail(" Error Occurred during the execution of   "+methodName +" Method from the class " +className,methodName,driver);
			System.out.println(e.getMessage());
		}
		
	}
	////////////////
	public void selectOption(WebDriver driver,WebElement element,String text,String methodName ,String className) throws Exception
	{
		try
		{
			Select dropDownSelection = new Select(element);
			dropDownSelection.selectByVisibleText(text);
			Thread.sleep(5000);
			extentreport.reportPass(" Successfully Executed "+methodName +" Method from the class " +className ,methodName,driver);
		}
		catch (Exception e) 
		{
			extentreport.reportFail(" Error Occurred during the execution of   "+methodName +" Method from the class " +className,methodName,driver);
			System.out.println(e.getMessage());
		}
		
	}
	/////////////////	
	public void writeText(WebDriver driver,WebElement element,String methodName ,String text,String className) throws Exception
	{
		try
		{
			element.clear();
			Thread.sleep(5000);
			element.sendKeys(text);
			Thread.sleep(5000);
			extentreport.reportPass(" Successfully Executed "+methodName +" Method from the class " +className ,methodName,driver);
		}
		catch (Exception e) 
		{
			extentreport.reportFail(" Error Occurred during the execution of   "+methodName +" Method from the class " +className,methodName,driver);
			System.out.println(e.getMessage());
		}
		
	}
	
	public String readText(WebDriver driver,WebElement element,String methodName,String className) throws Exception
	{
		
		try
		{
			gatheredText = element.getText().toString();
			extentreport.reportPass(" Successfully Executed "+methodName +" Method from the class " +className ,methodName,driver);
			
		}
		catch (Exception e) 
		{
			extentreport.reportFail(" Error Occurred during the execution of   "+methodName +" Method from the class " +className,methodName,driver);
			System.out.println(e.getMessage());
			return gatheredText;
		}
		
		return gatheredText;
	}
	
	public void mouseHoverAction(WebDriver driver,WebElement element,String methodName,String className) throws Exception
	{
		
		try
		{
			action.moveToElement(element).build().perform();
			extentreport.reportPass(" Successfully Executed "+methodName +" Method from the class " +className ,methodName,driver);
		}
		catch (Exception e) 
		{
			extentreport.reportFail(" Error Occurred during the execution of   "+methodName +" Method from the class " +className,methodName,driver);
			System.out.println(e.getMessage());
		}
		
	}
	
	public void getBrowserAndOsInfo(WebDriver driver,String browserName,String os,String version) throws Exception
	{
		cap = ((RemoteWebDriver) driver).getCapabilities();
	    browserName = cap.getBrowserName().toLowerCase();
	    os = cap.getPlatform().toString();
	    version = cap.getVersion().toString();
	    System.out.println("Executed on the OS :  "+os+" with the Browser : "+browserName+" : and Version is : "+version);
	    
	    try
		{
			extentreport.reportInfo("All the Test Cases are executed on the :  "+os+" OS with the : "+browserName+" : Browser and Version is : "+version, driver);
		}
		catch (Exception e) 
		{
			extentreport.reportInfo(" From Catch Block All the Test Cases are executed on the :  "+os+" OS with the : "+browserName+" : Browser and Version is : "+version, driver);
			System.out.println(e.getMessage());
		}
	    
	}
	
	public void verifyText(WebDriver driver,WebElement element,String methodName,String text,String className) throws Exception
	{
		try
		{
			if(element.getText().equalsIgnoreCase(text))
			{
				extentreport.reportPass("Successfully Verified the "+text+"from the "+className+" page",methodName,driver);						
			}			
		}
		catch (Exception e) 
		{
			extentreport.reportFail(" Error Occurred during the execution of   "+methodName +" Method from the class " +className,methodName,driver);
			System.out.println(e.getMessage());
		}
		
	}
	
	
	public WebDriver createDriver(String browserName) 
	{
        String s = browserName.toUpperCase();
        
        if (s.equals("CHROME")) 
        {
        	 
        	WebDriver driver;
        	
            ChromeOptions options = new ChromeOptions();
            options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,UnexpectedAlertBehaviour.ACCEPT);
            options.addArguments( "--start-maximized");
            
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\drivers\\chromedriver.exe");
            driver = new ChromeDriver(options);
            
           return driver;
         }
         else if (s.equals("FIREFOX"))
         {
             WebDriver driver;
             Proxy proxy = new Proxy();
             proxy.setAutodetect(true);

             FirefoxOptions firefoxOptions = new FirefoxOptions();
             firefoxOptions.setCapability("marionette", true);
             firefoxOptions.setCapability(CapabilityType.PROXY, proxy);
             firefoxOptions.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,UnexpectedAlertBehaviour.ACCEPT);
             
             System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\drivers\\geckodriver.exe");
             driver = new FirefoxDriver(firefoxOptions);
             driver.manage().window().maximize();
             driver.manage().timeouts().pageLoadTimeout(timeoutInSec, TimeUnit.SECONDS);
             driver.manage().timeouts().implicitlyWait(timeoutInSec, TimeUnit.SECONDS);
             return driver;
         }
         else
         {
        	 
         }
         return null;
     }
	
	public void selectValueFromDropDown(String value,String xp1,String xp2,WebDriver driver,List<WebElement> element,String methodName,String className)
	{
		for(int i=1;i<= element.size();i++)
		{
			if(driver.findElement(By.xpath(xp1+i+xp2)).getText().equalsIgnoreCase(value))
			{
				driver.findElement(By.xpath(xp1+i+xp2)).click();break;
			}
		}
	}
        
        
	}
	


