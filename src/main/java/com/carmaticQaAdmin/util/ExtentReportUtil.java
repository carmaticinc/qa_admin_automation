package com.carmaticQaAdmin.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReportUtil 
{

	
	protected  ExtentReports 		extent;
	protected  ExtentTest 			test;
	protected  ExtentHtmlReporter 	html;
	protected  String 				Screenshotlocation;
         
    
    public void startTest(String testName)
    {
        html	 = new ExtentHtmlReporter(System.getProperty("user.dir")+"\\Extent-Reports\\"+"ExtentReport.html");
       	extent 	 = new ExtentReports();	       	
    	extent.attachReporter(html);	        
        
    	html.loadXMLConfig(new File(System.getProperty("user.dir")+"\\src\\main\\resources\\configurations\\extent-config.xml"),true);    	
    	html.config().setAutoCreateRelativePathMedia(true);    	
    	test = extent.createTest(testName);
    	
    	/*It is also possible to configure the xlm info from the code as below,
    	you can use this method or like above using loadXMLConfig mehod
    	
    	html.config().setAutoCreateRelativePathMedia(true);
    	html.config().setCSS("css-string");
    	html.config().setDocumentTitle("page title");
    	html.config().setEncoding("utf-8");
    	html.config().setJS("js-string");
    	html.config().setProtocol(Protocol.HTTPS);
    	html.config().setReportName("build name");
    	html.config().setTheme(Theme.DARK);
    	html.config().setTimeStampFormat("MMM dd, yyyy HH:mm:ss");*/
    	
    }
    
    public void reportPass(String data,String imagePath,WebDriver driver) throws Exception
    {
    	Screenshotlocation = getScreenshot(driver,imagePath);
    	test.pass(data, MediaEntityBuilder.createScreenCaptureFromPath(Screenshotlocation).build());
    	//test.pass(data);
    }
    
    public void reportFail(String data,String imagePath,WebDriver driver) throws Exception
    {
    	Screenshotlocation = getScreenshot(driver,imagePath);
    	test.fail(data, MediaEntityBuilder.createScreenCaptureFromPath(Screenshotlocation).build());
    }
    
    public void reportSkip(String data,String imagePath,WebDriver driver) throws Exception
    {
    	test.log(Status.SKIP, data);
    	Screenshotlocation = getScreenshot(driver,imagePath);
    	test.skip(data, MediaEntityBuilder.createScreenCaptureFromPath(Screenshotlocation).build());
    }
    
    public void reportInfo(String data,WebDriver driver) throws Exception
    {
    	test.info(data);
    }
       
    public void endTest()
    {
        if(extent!=null)
       {
        	extent.flush();
       }
    	
    }

    public String getScreenshot(WebDriver driver, String screenshotName) throws Exception 
    {
    	 String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
    	 TakesScreenshot ts = (TakesScreenshot) driver;
    	 File source = ts.getScreenshotAs(OutputType.FILE);
    	 String destination = System.getProperty("user.dir") + "\\src\\test\\resources\\reports\\"+screenshotName+dateName+".png";
    	 File finalDestination = new File(destination);
    	 FileUtils.copyFile(source, finalDestination);
    	 return destination;
    }
  


}
